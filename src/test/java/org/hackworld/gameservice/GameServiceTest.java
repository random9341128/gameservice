package org.hackworld.gameservice;

import org.hackworld.gameservice.exceptions.DuplicatedGameException;
import org.hackworld.gameservice.exceptions.GameNotFoundException;
import org.hackworld.gameservice.model.Game;
import org.hackworld.gameservice.service.GameService;
import org.hackworld.gameservice.service.GameServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GameServiceTest {

    private GameService gameService;

    private static void assertThatGamesAttributesMatch(Game actual, Game expected) {
        assertThat(actual.getName(), is(expected.getName()));
        assertThat(actual.getCreationDate(), is(expected.getCreationDate()));
        assertThat(actual.getActive(), is(expected.getActive()));
    }

    @BeforeEach
    public void setUp() {
        gameService = new GameServiceImpl();
    }
    @Test
    public void weCanCreateAndRetrieveGames() {
        Game game1 = new Game("game1Name", ZonedDateTime.now(), true);
        Game game2 = new Game("game2Name", ZonedDateTime.now(), true);
        gameService.createGame(game1);
        gameService.createGame(game2);
        Game retrievedGame1 = gameService.getGame("game1Name");
        Game retrievedGame2 = gameService.getGame("game2Name");
        assertThat(retrievedGame1, notNullValue());
        assertThat(retrievedGame2, notNullValue());
        assertThatGamesAttributesMatch(retrievedGame1, game1);
        assertThatGamesAttributesMatch(retrievedGame2, game2);
    }

    @Test
    public void duplicatedGameCreationShouldThrowException() {
        Game game = new Game("game1Name", ZonedDateTime.now(), true);
        gameService.createGame(game);
        assertThrows(DuplicatedGameException.class, () -> gameService.createGame(game));
    }

    @Test
    public void weCanUpdateAnExistingGame() {
        Game game = new Game("gameName", ZonedDateTime.now(), true);
        gameService.createGame(game);
        Game updatedGame = new Game("gameName", ZonedDateTime.now().plusMinutes(10), false);
        gameService.updateGame(updatedGame);
        Game retrievedGame = gameService.getGame("gameName");
        assertThatGamesAttributesMatch(retrievedGame, updatedGame);
    }

    @Test
    public void updatingNonExistingGameShouldThrowException() {
        Game updatedGame = new Game("gameName", ZonedDateTime.now().plusMinutes(10), false);
        assertThrows(GameNotFoundException.class, () -> gameService.updateGame(updatedGame));
    }

    @Test
    public void weCanDeleteAnExistingGame() {
        Game game = new Game("gameName", ZonedDateTime.now(), true);
        gameService.createGame(game);
        gameService.deleteGame("gameName");
        Game retrievedGame = gameService.getGame("gameName");
        assertThat(retrievedGame, nullValue());
    }

    @Test
    public void deletingNonExistingGameShouldThrowException() {
        assertThrows(GameNotFoundException.class, () -> gameService.deleteGame("gameName"));
    }

    @Test
    public void weCanRetrieveAllGames() {
        Game game1 = new Game("game1Name", ZonedDateTime.now(), true);
        Game game2 = new Game("game2Name", ZonedDateTime.now(), true);
        Game game3 = new Game("game3Name", ZonedDateTime.now(), true);
        gameService.createGame(game1);
        gameService.createGame(game2);
        gameService.createGame(game3);
        Collection<Game> gamesList = gameService.getAllGames();
        assertThat(gamesList, containsInAnyOrder(game1, game2, game3));
    }

}
