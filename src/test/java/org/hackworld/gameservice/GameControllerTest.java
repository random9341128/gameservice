package org.hackworld.gameservice;

import org.hackworld.gameservice.exceptions.DuplicatedGameException;
import org.hackworld.gameservice.exceptions.GameNotFoundException;
import org.hackworld.gameservice.model.Game;
import org.hackworld.gameservice.service.GameService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZonedDateTime;
import java.util.List;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private GameService gameServiceMock;

    @Test
    public void getExistingGame() throws Exception {
        ZonedDateTime creationDate = ZonedDateTime.now();
        Game game = new Game("gameName", creationDate, true);
        when(gameServiceMock.getGame("gameName")).thenReturn(game);
        mockMvc.perform(get("/game/gameName").accept(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.name").value("gameName"))
               .andExpect(jsonPath("$.creationDate").value(creationDate.format(ISO_OFFSET_DATE_TIME)))
               .andExpect(jsonPath("$.active").value(Boolean.TRUE));
    }

    @Test
    public void getNonExistingGame() throws Exception {
        mockMvc.perform(get("/game/gameName").accept(MediaType.APPLICATION_JSON))
               .andExpect(status().isNotFound());
    }

    @Test
    public void createNewGame() throws Exception {
        ZonedDateTime creationDate = ZonedDateTime.now();
        Game game = new Game("gameName", creationDate, true);
        when(gameServiceMock.createGame(any(Game.class))).thenReturn(game);
        String formattedCreationDate = creationDate.format(ISO_OFFSET_DATE_TIME);
        mockMvc.perform(post("/game").accept(MediaType.APPLICATION_JSON)
                                     .contentType(MediaType.APPLICATION_JSON)
                                     .content("{\"name\":\"gameName\",\"creationDate\":\"" + formattedCreationDate + "\",\"active\":true}")
               )
               .andExpect(status().isCreated())
               .andExpect(jsonPath("$.name").value("gameName"))
               .andExpect(jsonPath("$.creationDate").value(formattedCreationDate))
               .andExpect(jsonPath("$.active").value(Boolean.TRUE));
    }

    @Test
    public void createDuplicatedGame() throws Exception {
        when(gameServiceMock.createGame(any(Game.class))).thenThrow(DuplicatedGameException.class);
        mockMvc.perform(post("/game").accept(MediaType.APPLICATION_JSON)
                                     .contentType(MediaType.APPLICATION_JSON)
                                     .content("{\"name\":\"gameName\",\"creationDate\":\"2023-07-11T12:11:14.234729521Z\",\"active\":true}")
               )
               .andExpect(status().isConflict());
    }

    @Test
    public void updateExistingGame() throws Exception {
        ZonedDateTime creationDate = ZonedDateTime.now();
        Game game = new Game("gameName", creationDate, true);
        when(gameServiceMock.updateGame(any(Game.class))).thenReturn(game);
        String formattedCreationDate = creationDate.format(ISO_OFFSET_DATE_TIME);
        mockMvc.perform(put("/game").accept(MediaType.APPLICATION_JSON)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content("{\"name\":\"gameName\",\"creationDate\":\"" + formattedCreationDate + "\",\"active\":true}")
               )
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.name").value("gameName"))
               .andExpect(jsonPath("$.creationDate").value(formattedCreationDate))
               .andExpect(jsonPath("$.active").value(Boolean.TRUE));
    }

    @Test
    public void updateNonExistingGameShouldReturnError() throws Exception {
        when(gameServiceMock.updateGame(any(Game.class))).thenThrow(GameNotFoundException.class);
        mockMvc.perform(put("/game").accept(MediaType.APPLICATION_JSON)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content("{\"name\":\"gameName\",\"creationDate\":\"2023-07-11T12:11:14.234729521Z\",\"active\":true}")
               )
               .andExpect(status().isNotFound());
    }

    @Test
    public void deleteExistingGame() throws Exception {
        ZonedDateTime creationDate = ZonedDateTime.now();
        Game game = new Game("gameName", creationDate, true);
        when(gameServiceMock.updateGame(any(Game.class))).thenReturn(game);
        mockMvc.perform(delete("/game/gameName").accept(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk());
    }

    @Test
    public void deleteNonExistingGameShouldReturnError() throws Exception {
        doThrow(GameNotFoundException.class).when(gameServiceMock).deleteGame("gameName");
        mockMvc.perform(delete("/game/gameName").accept(MediaType.APPLICATION_JSON))
               .andExpect(status().isNotFound());
    }

    @Test
    public void getAllGames() throws Exception {
        Game game1 = new Game("game1Name", ZonedDateTime.now(), true);
        Game game2 = new Game("game2Name", ZonedDateTime.now(), true);
        Game game3 = new Game("game3Name", ZonedDateTime.now(), false);
        List<Game> gamesList = List.of(game1, game2, game3);
        when(gameServiceMock.getAllGames()).thenReturn(gamesList);
        mockMvc.perform(get("/game").accept(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.length()").value(3));
    }

}
