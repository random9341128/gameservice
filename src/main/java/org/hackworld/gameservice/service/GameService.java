package org.hackworld.gameservice.service;

import org.hackworld.gameservice.model.Game;

import java.util.Collection;

public interface GameService {

    Game createGame(Game game);

    Game getGame(String gameName);

    Game updateGame(Game game);

    void deleteGame(String gameName);

    Collection<Game> getAllGames();
}
