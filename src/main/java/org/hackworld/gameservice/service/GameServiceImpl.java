package org.hackworld.gameservice.service;

import org.hackworld.gameservice.exceptions.DuplicatedGameException;
import org.hackworld.gameservice.exceptions.GameNotFoundException;
import org.hackworld.gameservice.model.Game;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class GameServiceImpl implements GameService {

    private final Map<String, Game> gameStore = new ConcurrentHashMap<>();

    public Game createGame(Game game) {
        checkGameDoesntExist(game.getName());
        gameStore.put(game.getName(), game);
        return game;
    }

    public Game getGame(String gameName) {
        return gameStore.get(gameName);
    }

    @Override
    public Game updateGame(Game game) {
        checkGameExists(game.getName());
        gameStore.put(game.getName(), game);
        return game;
    }

    @Override
    public void deleteGame(String gameName) {
        checkGameExists(gameName);
        gameStore.remove(gameName);
    }

    @Override
    public Collection<Game> getAllGames() {
        return gameStore.values();
    }

    private void checkGameExists(String gameName) {
        if (!gameStore.containsKey(gameName)) throw new GameNotFoundException();
    }

    private void checkGameDoesntExist(String gameName) {
        if (gameStore.containsKey(gameName)) throw new DuplicatedGameException();
    }
}
