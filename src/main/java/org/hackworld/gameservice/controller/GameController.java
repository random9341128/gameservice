package org.hackworld.gameservice.controller;

import org.hackworld.gameservice.exceptions.GameNotFoundException;
import org.hackworld.gameservice.model.Game;
import org.hackworld.gameservice.service.GameService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/game")
public class GameController {

    private final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping("/{gameName}")
    public ResponseEntity<Game> getGame(@PathVariable String gameName){
        Game game = gameService.getGame(gameName);
        if (game == null) throw new GameNotFoundException();
        return ResponseEntity.status(HttpStatus.OK).body(game);
    }

    @PostMapping
    public ResponseEntity<Game> createGame(@RequestBody Game game) {
        Game createdGame = gameService.createGame(game);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdGame);
    }

    @PutMapping
    public ResponseEntity<Game> updateGame(@RequestBody Game game) {
        Game updatedGame = gameService.updateGame(game);
        return ResponseEntity.status(HttpStatus.OK).body(updatedGame);
    }

    @DeleteMapping("/{gameName}")
    public ResponseEntity<Game> deleteGame(@PathVariable String gameName){
        gameService.deleteGame(gameName);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping
    public ResponseEntity<Collection<Game>> getAllGames(){
        Collection<Game> games = gameService.getAllGames();
        return ResponseEntity.status(HttpStatus.OK).body(games);
    }

}
