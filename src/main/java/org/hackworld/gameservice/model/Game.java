package org.hackworld.gameservice.model;

import java.time.ZonedDateTime;

public class Game {

    private String name;
    private ZonedDateTime creationDate;
    private Boolean active;

    public Game(String name, ZonedDateTime creationDate, Boolean active) {
        this.name = name;
        this.creationDate = creationDate;
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
