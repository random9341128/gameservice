package org.hackworld.gameservice.exceptions;

import org.hackworld.gameservice.exceptions.DuplicatedGameException;
import org.hackworld.gameservice.exceptions.GameNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({DuplicatedGameException.class})
    public ResponseEntity<?> handleDuplicatedGameException(DuplicatedGameException e) {
        return ResponseEntity.status(HttpStatus.CONFLICT).build();
    }

    @ExceptionHandler({GameNotFoundException.class})
    public ResponseEntity<?> handleDuplicatedGameException(GameNotFoundException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
