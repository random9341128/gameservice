
URL=http://localhost:8080/game


echo

echo Adding games
curl -s -X POST $URL -H "Content-Type: application/json" -d '{"name": "game1", "creationDate": "2023-07-11T13:46:18.312+00:00", "active": true}' | jq
curl -s -X POST $URL -H "Content-Type: application/json" -d '{"name": "game2", "creationDate": "2023-07-11T13:47:18.312+00:00", "active": false}' | jq
curl -s -X POST $URL -H "Content-Type: application/json" -d '{"name": "game3", "creationDate": "2023-07-11T13:48:18.312+00:00", "active": true}' | jq
echo Retrieving all games
curl -s $URL | jq
echo Retrieving one game
curl -s $URL/game1 | jq
echo Deleting a game
curl -s -X DELETE $URL/game2
echo Updating a game
curl -s -X PUT $URL -H "Content-Type: application/json" -d '{"name": "game3", "creationDate": "2023-07-11T13:48:18.312+00:00", "active": false}' | jq
echo Retrieving all games again
curl -s $URL | jq
echo Deleting the other games
curl -s -X DELETE $URL/game1
curl -s -X DELETE $URL/game3
