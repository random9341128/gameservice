# Task
Create an application to manage games within a game service provider environment. The application should be developed using Java, Spring, and Maven.

To begin, define an interface called GameService that will handle CRUD operations related to games. A game is a simple object with unique name, creation date, and active status.

Next, implement the GameService interface and create a simple in-memory cache structure to store the games. This cache should take into account concurrency and potential performance issues.

Expose REST endpoints to utilize the implemented GameService within the Spring application.

# Notes

* The use of the term "cache" may create confusion. As nothing is mentioned about a separate persistent storage solution or any requirements for the cache like size, eviction or replacement policies, etc, I'm assuming that "cache" refers to an in-memory storage mechanism and not a traditional cache with a database backend.


* I'm going to try to follow a TDD with tests written first approach.


* As the logic is limited to CRUD and thus very simple I've kept the gameStore inside the GameServiceImpl. If we were to have a more complex business logic and/or we were going to provide multiple mechanisms of storage I'd separate both things, by creating some kind of DataStore interface, with one or more implementations. 


* With the current implementation the Game class could be made immutable or even converted to a record, depending on how likely is that in the future some JPA-based persistence implementation is needed.


* I'm going to assume that trying to update a non-existing game should return an error and not just silently create it.


* As the result of following a TDD approach we ended with two very similar tests for the GameService: weCanCreateAndRetrieveOneGame and weCanCreateAndRetrieveTwoGames. After some tidying-up they were merged into weCanCreateAndRetrieveGames.


* Maybe a few more tests could be added, starting the web environment fully instead of using MockMvc. I've run a few tests with curl and everything looks okay


* We could also check the endpoints parameters and improve the error messages on malformed inputs


* I implemented the REST API for the same CRUD operations. For a more serious use I'd have probably implemented an alternative API with functionalities like creating new game only passing the game name, retrieving game details and ending (de-activating) a game.  


 